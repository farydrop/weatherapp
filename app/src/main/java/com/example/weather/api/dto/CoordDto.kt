package com.example.weather.api.dto

import com.google.gson.annotations.SerializedName

data class CoordDto(
    @SerializedName("ion") val ion: Double,
    @SerializedName("lat") val lat: Double
)
