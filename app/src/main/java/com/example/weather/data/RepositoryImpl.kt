package com.example.weather.data

import com.example.weather.domain.CoroutineScopeImpl

abstract class RepositoryImpl : Repository, CoroutineScopeImpl() {
}