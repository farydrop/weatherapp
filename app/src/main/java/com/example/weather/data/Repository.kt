package com.example.weather.data

import com.example.weather.domain.Cancellable

interface Repository : Cancellable {
}