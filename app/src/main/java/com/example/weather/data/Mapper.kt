package com.example.weather.data

import com.example.weather.api.dto.ResponseDto

abstract class Mapper<From : ResponseDto, To> {
    abstract fun map(from: From): To
}