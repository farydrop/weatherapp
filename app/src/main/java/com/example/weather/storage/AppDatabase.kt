package com.example.weather.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.weather.storage.dao.WeatherDao
import com.example.weather.storage.entity.WeatherEntity


@Database(version = 1, exportSchema = false, entities = [WeatherEntity::class])
abstract class AppDatabase : RoomDatabase(){
    abstract fun weatherDao(): WeatherDao
}