package com.example.weather.storage.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.weather.storage.entity.WeatherEntity

@Dao
interface WeatherDao : BaseDao<WeatherEntity> {
    @Query("SELECT * FROM weather WHERE id=:id")
    fun getWeather(id: Long): WeatherEntity?
}