package com.example.weather.domain

interface Interactor<T : InteractorOut> : Cancellable {

    fun setupInteractorOut(out: T)

}