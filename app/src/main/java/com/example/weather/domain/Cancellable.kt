package com.example.weather.domain

interface Cancellable {
    fun cancel()
}