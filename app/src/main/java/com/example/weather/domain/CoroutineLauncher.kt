package com.example.weather.domain

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.isActive

interface CoroutineLauncher: CoroutineScope {

    fun CoroutineLauncher.launchSafely(
        onLoading: (Boolean) -> Unit,
        onError: ((Throwable) -> Unit)? = null,
        block: suspend  CoroutineScope.() -> Unit
    ): Job = Launch {
        var isCancellation = false
        onLoading(true)
        try {
            block()
        } catch (error: Throwable) {
            error.printStackTrace()
            isCancellation = error is CancellationException
            if (!isActive) return@Launch

            onError?.invoke(error)

        } finally {
            if (!isCancellation) onLoading(false)
        }
    }

}