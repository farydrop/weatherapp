package com.example.weather.di

import android.content.Context
import com.example.weather.api.Api
import com.example.weather.storage.AppDatabase
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Component(modules = [NetworkModule::class, StorageModule::class])
@Singleton
interface AppComponent {

    fun context(): Context
    fun api(): Api
    fun appDatabase(): AppDatabase

    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        @BindsInstance
        fun context(context: Context): Builder
    }
}