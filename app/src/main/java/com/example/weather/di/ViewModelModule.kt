package com.example.weather.di

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import java.util.prefs.PreferencesFactory


@Module
interface ViewModelModule {
    @ScreenScope
    @Binds
    fun bindsViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}