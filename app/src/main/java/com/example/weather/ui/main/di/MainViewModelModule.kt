package com.example.weather.ui.main.di

import androidx.lifecycle.ViewModel
import com.example.weather.di.ScreenScope
import com.example.weather.di.ViewModelKey
import com.example.weather.di.ViewModelModule
import com.example.weather.ui.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
interface MainViewModelModule : ViewModelModule {
    @ScreenScope
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindsMainViewModel(viewModel: MainViewModel): ViewModel
}