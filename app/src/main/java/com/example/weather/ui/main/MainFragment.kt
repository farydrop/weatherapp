package com.example.weather.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.weather.App
import com.example.weather.ICON_URL
import com.example.weather.R
import com.example.weather.ui.main.di.MainComponent
import com.example.weather.utils.observe
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.main_fragment.*
import javax.inject.Inject

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private val viewModel: MainViewModel by viewModels { factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainComponent
            .init((requireActivity().application as App).appComponent)
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        srlRefresh.setColorSchemeResources(R.color.colorAccent)
        srlRefresh.setOnRefreshListener { viewModel.onRefresh() }


        observe(viewModel.loading){
            srlRefresh.isRefreshing = it
        }

        observe(viewModel.error){
            Snackbar.make(view, getString(R.string.error_network), Snackbar.LENGTH_SHORT)
                .show()

        }

        observe(viewModel.data){
            tvCityName.text = it.name
            tvTemp.text = getString(R.string.temp, it.temp)
            tvTfeelsTemp.text = getString(R.string.temp, it.feelsLike)

            Picasso.get()
                .load(String.format(ICON_URL, it.icon))
                .into(ivWeather)

        }

    }

}