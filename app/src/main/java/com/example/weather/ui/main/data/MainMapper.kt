package com.example.weather.ui.main.data

import com.example.weather.api.dto.CityWeatherDto
import com.example.weather.data.Mapper
import com.example.weather.ui.main.domain.WeatherModel

class MainMapper: Mapper<CityWeatherDto, WeatherModel>() {
    override fun map(from: CityWeatherDto): WeatherModel{
        return WeatherModel(
            from.id,
            from.name,
            from.weather[0].icon,
            from.main.temp,
            from.main.feels_like
        )
    }
}