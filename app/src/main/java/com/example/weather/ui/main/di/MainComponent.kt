package com.example.weather.ui.main.di

import com.example.weather.di.AppComponent
import com.example.weather.di.ScreenScope
import com.example.weather.ui.main.MainFragment
import dagger.Component


@ScreenScope
@Component(
    dependencies = [AppComponent::class],
    modules = [MainViewModelModule::class, MainModule::class]
)
interface MainComponent {
    fun inject(mainFragment: MainFragment)

    @Component.Builder
    interface Builder{
        fun appComponent(appComponent: AppComponent): Builder

        fun build(): MainComponent
    }

    companion object {
        fun init(appComponent: AppComponent): MainComponent{
            return DaggerMainComponent
                .builder()
                .appComponent(appComponent)
                .build()
        }
    }
}

