package com.example.weather.ui.main.data

import com.example.weather.Units
import com.example.weather.api.Api
import com.example.weather.data.RepositoryImpl
import com.example.weather.storage.dao.WeatherDao
import com.example.weather.storage.entity.WeatherEntity
import com.example.weather.ui.main.domain.MainRepo
import com.example.weather.ui.main.domain.WeatherModel
import javax.inject.Inject

class MainRepoImpl @Inject constructor(
    private val dataBase: WeatherDao,
    private val api: Api
): RepositoryImpl(), MainRepo {

    override suspend fun loadWeather(cityId: Int) = ioAsync {
        MainMapper().map(api.getWeather(cityId, Units.METRIC))
            .apply {
            dataBase.insert(WeatherEntity(id,name,icon,temp,feelsLike))
        }
    }

    override suspend fun getWeather(cityId: Int): WeatherModel? = ioAsync{
        dataBase.getWeather(cityId)?.run { WeatherModel(id,name,icon,temp,feels_like) }
    }
}