package com.example.weather.ui.main.domain

import com.example.weather.domain.Interactor
import com.example.weather.domain.InteractorOut

interface MainInteractorOut : InteractorOut {

    fun isLoading(loading: Boolean)
    fun onLoaded(weather: WeatherModel)
    fun onError(e: Throwable)

}