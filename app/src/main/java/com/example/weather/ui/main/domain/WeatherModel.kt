package com.example.weather.ui.main.domain

data class WeatherModel(
    val id: Int,
    val name: String,
    val icon: String,
    val temp: Double,
    val feelsLike: Double
)
