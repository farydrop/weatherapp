package com.example.weather.ui.main.di

import com.example.weather.api.Api
import com.example.weather.di.ScreenScope
import com.example.weather.storage.AppDatabase
import com.example.weather.storage.dao.WeatherDao
import com.example.weather.ui.main.data.MainRepoImpl
import com.example.weather.ui.main.domain.MainInteractor
import com.example.weather.ui.main.domain.MainInteractorImpl
import com.example.weather.ui.main.domain.MainRepo
import dagger.Provides

class MainModule {

    @ScreenScope
    @Provides
    fun provideWeatherDao(appDatabase: AppDatabase): WeatherDao =
        appDatabase.weatherDao()

    @ScreenScope
    @Provides
    fun provideMainRepo(api: Api, dataBase: WeatherDao): MainRepo =
        MainRepoImpl(dataBase, api)

    @ScreenScope
    @Provides
    fun provideMainInteractor(repo: MainRepo): MainInteractor =
        MainInteractorImpl(repo)

}