package com.example.weather.ui.main.domain

import com.example.weather.domain.Interactor

interface MainInteractor : Interactor<MainInteractorOut> {
    fun loadWeather()
}