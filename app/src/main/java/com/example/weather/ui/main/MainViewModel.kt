package com.example.weather.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import com.example.weather.ui.main.domain.MainInteractor
import com.example.weather.ui.main.domain.MainInteractorOut
import com.example.weather.ui.main.domain.WeatherModel
import com.example.weather.utils.SingleLiveEvent
import javax.inject.Inject

class MainViewModel @Inject constructor(val interactor: MainInteractor) : ViewModel(),
    MainInteractorOut {
    private val loadingMut = MutableLiveData<Boolean>()
    private val errorMut: MutableLiveData<Unit> = SingleLiveEvent()
    private val dataMut = MutableLiveData<WeatherModel>()

    val loading: LiveData<Boolean> get() = loadingMut
    val error: LiveData<Unit> get() = errorMut
    val data: LiveData<WeatherModel> get() = dataMut

    init {
        interactor.setupInteractorOut(this)
        interactor.loadWeather()
    }

    fun onRefresh() {
        interactor.loadWeather()
    }

    override fun isLoading(loading: Boolean) {
        loadingMut.value = loading
    }

    override fun onLoaded(weather: WeatherModel) {
        dataMut.value = weather
    }

    override fun onError(e: Throwable) {
        errorMut.value = Unit
    }
}