package com.example.weather.ui.main.domain

import com.example.weather.CITY_ID
import com.example.weather.domain.InteractorImpl
import javax.inject.Inject

class MainInteractorImpl @Inject constructor(private val repo: MainRepo) : InteractorImpl<MainInteractorOut>(), MainInteractor{
    override fun loadWeather() {
        launchSafely(
            { out.isLoading(it) },
            { out.onError(it) }) {
            repo.getWeather(CITY_ID)?.let { out.onLoaded(it) }
            out.onLoaded(repo.loadWeather(CITY_ID))
        }
    }
}