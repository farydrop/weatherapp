package com.example.weather.ui.main.domain

import com.example.weather.domain.Repository

interface MainRepo : Repository {
    suspend fun loadWeather(cityId: Int): WeatherModel
    suspend fun getWeather(cityId: Int): WeatherModel?
}